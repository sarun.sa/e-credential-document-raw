# e-credential-document-raw

| No.            | lab result     | Vaccine | consent |    tmlt    |    lab status     |
|----------------|----------------|---------|---------|------------|-------------------|
| 0000000000001  | positive       | 1 dose  |  false  |   350501   |  RT_PCR_DETECTED  |
| 0000000000002  | detectable     | 1 dose  |  false  |   350502   |  RT_PCR_DETECTED  |
| 0000000000003  | detected       | 1 dose  |  false  |   350503   |  RT_PCR_DETECTED  |
| 0000000000004  | reactive       | 1 dose  |  false  |   350506   |  ATK_DETECTED     |
| 0000000000005  | negative       | 1 dose  |  false  |   350507   |  ATK_DETECTED     |
| 0000000000006  | not detected   | 1 dose  |  false  |   350508   |  ATK_DETECTED     |
| 0000000000007  | undetectable   | 1 dose  |  false  |   350509   |  ATK_DETECTED     |
| 0000000000008  | not detectable | 1 dose  |  false  |   350506   |  ATK_DETECTED     |
| 0000000000009  | expire         | 1 dose  |  false  |   350506   |  EXPIRED          |
| 0000000000010  | positive       | 2 dose  |  false  |   350501   |  RT_PCR_DETECTED  |
| 0000000000011  | detected       | 2 dose  |  false  |   350502   |  RT_PCR_DETECTED  |
| 0000000000012  | detectable     | 2 dose  |  false  |   350503   |  RT_PCR_DETECTED  |
| 0000000000013  | reactive       | 2 dose  |  false  |   350506   |  ATK_DETECTED     |
| 0000000000014  | negative       | 2 dose  |  false  |   350507   |  ATK_DETECTED     |
| 0000000000015  | not detected   | 2 dose  |  false  |   350508   |  ATK_DETECTED     |
| 0000000000016  | undetectable   | 2 dose  |  false  |   350509   |  ATK_DETECTED     |
| 0000000000017  | not detectable | 2 dose  |  false  |   350506   |  ATK_DETECTED     |
| 0000000000018  | expire         | 2 dose  |  false  |   350506   |  EXPIRED          |
| 0000000000019  | no data        | 2 dose  |  false  |      -     |  NO_DATA          |
| 0000000000020  | no data        | 1 dose  |  false  |      -     |  NO_DATA          |
| 0000000000021  | positive       | 1 dose  |  true   |   350501   |  RT_PCR_DETECTED  |
| 0000000000022  | detectable     | 1 dose  |  true   |   350502   |  RT_PCR_DETECTED  |
| 0000000000023  | detected       | 1 dose  |  true   |   350503   |  RT_PCR_DETECTED  |
| 0000000000024  | reactive       | 1 dose  |  true   |   350506   |  ATK_DETECTED     |
| 0000000000025  | negative       | 1 dose  |  true   |   350507   |  ATK_DETECTED     |
| 0000000000026  | not detected   | 1 dose  |  true   |   350508   |  ATK_DETECTED     |
| 0000000000027  | undetectable   | 1 dose  |  true   |   350509   |  ATK_DETECTED     |
| 0000000000028  | not detectable | 1 dose  |  true   |   350506   |  ATK_DETECTED     |
| 0000000000029  | expire         | 1 dose  |  true   |   350506   |  EXPIRED          |
| 0000000000030  | positive       | 2 dose  |  true   |   350501   |  RT_PCR_DETECTED  |
| 0000000000031  | detected       | 2 dose  |  true   |   350502   |  RT_PCR_DETECTED  |
| 0000000000032  | detectable     | 2 dose  |  true   |   350503   |  RT_PCR_DETECTED  |
| 0000000000033  | reactive       | 2 dose  |  true   |   350506   |  ATK_DETECTED     |
| 0000000000034  | negative       | 2 dose  |  true   |   350507   |  ATK_DETECTED     |
| 0000000000035  | not detected   | 2 dose  |  true   |   350508   |  ATK_DETECTED     |
| 0000000000036  | undetectable   | 2 dose  |  true   |   350509   |  ATK_DETECTED     |
| 0000000000037  | not detectable | 2 dose  |  true   |   350506   |  ATK_DETECTED     |
| 0000000000038  | expire         | 2 dose  |  true   |   350506   |  EXPIRED          |
| 0000000000039  | no data        | 2 dose  |  true   |      -     |  NO_DATA          |
| 0000000000040  | no data        | 1 dose  |  true   |      -     |  NO_DATA          |
| 0000000000041  | 3DAYS_NOT_DETECTED   | 1 dose  |  true   |   350501   | RT_PCR_DETECTED   |
| 0000000000042  | 3DAYS_NOT_DETECTED   | 2 dose  |  true   |   350506   | ATK_DETECTED      |
| 0000000000043  | 3DAYS_NOT_DETECTED   | 1 dose  |  false  |   350506   | ATK_DETECTED      |
| 0000000000044  | 3DAYS_NOT_DETECTED   | 2 dose  |  false  |   350501   | RT_PCR_DETECTED   |
| 0000000000045  | negative   | 2 dose  |  true  |   350501   | RT_PCR_DETECTED   |